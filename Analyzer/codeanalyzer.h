#ifndef CODEANALYZER_H
#define CODEANALYZER_H

#include <QObject>

class CodeAnalyzer : public QObject
{
    Q_OBJECT
public:
    explicit CodeAnalyzer(const int  language, QObject *parent = 0);
    ~CodeAnalyzer();
signals:
    void    requestNextCodeBlock    ();
    void    writeNextCodeBlock      (const QString &nextCodeBlock);
    void    criticalError           (const int errorCode);
public slots:
    void    acceptNextCodeBlock     (const QString &codeBlock);
private:
    bool    analize (const QString &codeBlock);
    const   QString format(const QString &codeBlock);
};

#endif // CODEANALYZER_H
