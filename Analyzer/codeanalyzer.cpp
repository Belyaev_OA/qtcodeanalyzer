#include "codeanalyzer.h"
#include "service.h"
CodeAnalyzer::CodeAnalyzer(const int language, QObject *parent)
    : QObject(parent)
{

}

CodeAnalyzer::~CodeAnalyzer()
{

}
void CodeAnalyzer::acceptNextCodeBlock(const QString &codeBlock)
{
    analize(codeBlock);
}

bool CodeAnalyzer::analize(const QString &codeBlock)
{
    return !codeBlock.isEmpty();
}

const QString CodeAnalyzer::format(const QString &codeBlock)
{
    return QString(codeBlock);
}

