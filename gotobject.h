#ifndef GOTOBJECT_H
#define GOTOBJECT_H

#include <QObject>
#include <QString>
#include "Readers/filereader.h"
#include "Writers/filewriter.h"
#include "Analyzer/codeanalyzer.h"
class GotObject : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief GotObject
     * @param language
     * @param inputFileName
     * @param outputFileName
     * @param parent
     */
    explicit GotObject(const QString language, const QString &inputFileName,
                       const QString &outputFileName,
                       QObject *parent = 0);
    ~GotObject();
signals:
    void criticalError      (const int errorCode);
public slots:
    void slotCriticalError  (const int errorCode);
private:
    FileReader      *fileReader;
    FileWriter      *fileWriter;
    CodeAnalyzer    *codeAnalyzer;

private:
    void initAllObjects     (const QString language, const QString &inputFileName,
                             const QString &outputFileName);
    void createConnections  ();
};

#endif // GOTOBJECT_H
