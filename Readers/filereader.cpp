#include "filereader.h"
#include "service.h"
FileReader::FileReader(const QString &inputFileName, QObject *parent)
    : QObject(parent), inputFile(inputFileName)
{
    inputFile.exists() ? readNewBlock() :
                         criticalError(CRITICAL_ERROR_TYPE::ERROR_READING_FILE);
}

void FileReader::readNewBlock()
{
    inputFile.bytesAvailable() ?
                getNewCodeBlock( QString( inputFile.readLine(80) ) ) :
                endFile();
}

