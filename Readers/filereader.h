#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QTextStream>
#include <QFile>

class FileReader : public QObject
{
    Q_OBJECT
public:
    explicit FileReader(const QString &inputFileName, QObject *parent = 0);

signals:
    void getNewCodeBlock(const QString &block);
    void criticalError  (const int errorCode);
    void endFile        ();
public slots:
    void readNewBlock   ();

private:
    QFile inputFile;
};

#endif // FILEREADER_H
