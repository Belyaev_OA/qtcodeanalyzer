#-------------------------------------------------
#
# Project created by QtCreator 2015-09-20T19:44:03
#
#-------------------------------------------------

QT       += core

QT       -= gui
QMAKE_CXXFLAGS += -std=c++0x
TARGET = QtCodeAnalyzer
CONFIG   += console
#CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Readers/filereader.cpp \
    Writers/filewriter.cpp \
    Analyzer/codeanalyzer.cpp \
    gotobject.cpp

HEADERS += \
    Readers/filereader.h \
    Writers/filewriter.h \
    Analyzer/codeanalyzer.h \
    gotobject.h \
    service.h
