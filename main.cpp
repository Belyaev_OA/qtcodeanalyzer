#include <QCoreApplication>
#include "gotobject.h"
#include <iostream>
#include <QObject>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if(argc < 3){
        std::cout << "incorrect number of parameters\n";
        return 1;
    }
    GotObject *gotObject = new GotObject(argv[1], argv[2], argv[3]);
    return a.exec();
}
