#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QTextStreamManipulator>
class FileWriter : public QObject
{
    Q_OBJECT
public:
    explicit FileWriter(const QString &outputFileName, QObject *parent = 0);
signals:
    void criticalError  (const int      errorCode);
public slots:
    void writeNextBlock (const QString  &nextBlock);
private:
    QFile outFule;
};

#endif // FILEWRITER_H
