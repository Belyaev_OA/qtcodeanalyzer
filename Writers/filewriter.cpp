#include "filewriter.h"
#include "service.h"
FileWriter::FileWriter(const QString &outputFileName, QObject *parent)
    : QObject(parent), outFule(outputFileName)
{
    if(!outFule.exists()){
        emit criticalError(CRITICAL_ERROR_TYPE::ERROR_WRITENG_FILE);
    }
}

void FileWriter::writeNextBlock(const QString &nextBlock)
{
    outFule.write(nextBlock.toLocal8Bit());
}

