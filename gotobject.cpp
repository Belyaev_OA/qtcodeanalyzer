#include "gotobject.h"
#include "service.h"
GotObject::GotObject(const QString language, const QString &inputFileName,
                     const QString &outputFileName, QObject *parent) : QObject(parent)
{
    initAllObjects(language,inputFileName,outputFileName);
}

GotObject::~GotObject()
{
    delete  fileReader;
    delete  codeAnalyzer;
    delete  fileWriter;
}

void GotObject::slotCriticalError(const int errorCode)
{
    switch (errorCode) {
    case ERROR_READING_FILE:{

        break;
    }
    case ERROR_WRITENG_FILE:{

        break;
    }
    case FORMATTING_ERROR:{

        break;
    }
    case ERROR_ANALISIS:{

        break;
    }
    default:
        break;
    }
}

void GotObject::initAllObjects(const QString language, const QString &inputFileName,
                               const QString &outputFileName)
{
    QString str     = language;
    fileReader      = new FileReader(inputFileName,this);
    codeAnalyzer    = new CodeAnalyzer(str.toInt());
    fileWriter      = new FileWriter(outputFileName);
}

void GotObject::createConnections()
{
    connect(fileReader,SIGNAL(getNewCodeBlock( QString ) ),
            codeAnalyzer,SLOT(acceptNextCodeBlock( QString ) ) );

    connect(codeAnalyzer,SIGNAL(requestNextCodeBlock() ),
            fileReader,SLOT(readNewBlock() ) );

    connect(codeAnalyzer,SIGNAL(writeNextCodeBlock( QString ) ),
            fileWriter,SLOT(writeNextBlock( QString ) ) );

    connect(fileReader, SIGNAL(criticalError( int ) ),
            this, SLOT(slotCriticalError( int ) ) );

    connect(fileWriter,SIGNAL(criticalError( int ) ),
            this,SLOT(slotCriticalError( int ) ) );

    connect(codeAnalyzer,SIGNAL(criticalError( int ) ),
            this,SLOT(slotCriticalError( int ) ) );

}

